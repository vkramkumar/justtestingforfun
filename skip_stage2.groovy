node () {

stage ('Clean') {
    sh "rm -rf ${WORKSPACE}/*"
    
}


stage('test DEV'){  

    if(params.DEV.toBoolean()){  

        echo "Yes equal - running the DEV"
        sh "touch dev"
    } else {
        echo "Not equal - skipping the DEV"
    }
}


stage('test QA'){  
    if(params.QA.toBoolean()){  
    
        echo "Yes equal - running the QA"
        sh "touch QA"
    } else {
        echo "Not equal - skipping the QA"
    }
}

stage('test STAGE'){  
    if(params.STAGE.toBoolean()){  
        echo "Yes equal - running the STAGE"
        sh "touch STAGE"
    } else {
        echo "Not equal - skipping the STAGE"
    }
}

stage('test PERF'){  


      
    if(params.PERF.toBoolean()){  
        echo "Yes equal - running the PERF"
        sh "touch PERF"
    } else {
        echo "Not equal - skipping the PERF"
    }
}

}