node ('master') {
    
//    properties([parameters([string(
//        defaultValue: 'Hello',
//        description: 'How should I greet the world?',
//        name: 'Greeting')])])
  
  
properties([
  parameters([
    string(name: 'BRANCH_REPO_NAME', defaultValue: 'data', description: 'Enter branch name please.'),
//    choice(name: 'size', choices: ['S', 'M', 'L'],  description: 'Size choice (S/M/L)')
  ])
  ])


//  properties([
//  parameters([
//    choice(name: 'size', choices: ['S', 'M', 'L'],  description: 'Size choice (S/M/L)')
// ])
// ])

    def server = "PROD"    
    def path = "/web/cdl1"
    

//    env.name = 'PROD2'
    
    stage ('Clean') {
    sh "rm -rf *"
    echo "${params.Greeting} World!"
    }   
    
//    stage ('GIT_SCM'){
//        //branches : '${BRANCH_NAME}'
//       git url: 'https://vkramkumar@bitbucket.org/vkramkumar/justtestingforfun.git' 
//                branches : '${BRANCH_NAME}'
//        echo "Pullind code from  ${BRANCH_NAME} branch."
//        }


stage ('GIT_SCCM') {
BRANCH_REPO_NAME = "*/" + BRANCH_REPO_NAME
                        checkout([
                                $class                           : 'GitSCM',
                                branches                         : [[name: '$BRANCH_REPO_NAME']],
                                doGenerateSubmoduleConfigurations: false,
                                extensions                       : [[
                                                                            $class           : 'RelativeTargetDirectory',
                                                                            relativeTargetDir: '']],
                                submoduleCfg                     : [],
                                userRemoteConfigs                : [[
                                                                           url: 'https://vkramkumar@bitbucket.org/vkramkumar/justtestingforfun.git'
                                                                    ]]])
}

    
    stage ('Level_1') {
    mylevel1 = "OneTwoThree"
    echo "\u23E9\u23E9\u23E9\u23E9\u23E9 Running STAGE 1"
    echo "\u23E9\u23E9\u23E9\u23E9\u23E9 Coping data to ${server} at path : ${path} from ${BRANCH_REPO_NAME}"
    echo "${mylevel1}"
    }
    
    stage ('SHELL') {
    echo "\u26A1 \u26A1 \u26A1 \u26A1 \u26A1  Running STAGE SHELL"
    sh 'sh bash.sh'
    }
    
    
    stage ('shell out') {
    echo "\u231B\u231B\u231B\u231B\u231B\u231B  Running STAGE SHELL  OUT"
    LS = "${sh(script:'sh bash.sh', returnStdout: true).trim()}"
    echo "${LS}"
    if (LS.contains("Vikram Kumar")){
    echo "I got it, Vikram Kumar"
    }
    else {
    echo "Seems a error message"
    }
    
    }
    
    stage ('Data File 1') {
    sh "touch ${server}.txt"
    }
    
    stage ("Permissions") {
    sh "chmod 777 PROD.txt"
    }
}


node {
    def server = "DEV"
    def path = "/web/cdl2"
    def files = "DEV.txt PROD.txt"
    stage ('Level_2') {
        echo "Coping data to ${server} at path : ${path}"
    }
    stage ('Data File 1') {
    sh "touch ${server}.txt"
    echo "\u23E9\u23E9\u23E9\u23E9  ${mylevel1}"
    }
    
    stage ("Permissions") {
    sh "chmod 777 ${files}"

//    Use to exit the build or Mark as failure
//1   sh "exit 1"
//2   assert condition : "Build fails because..."
//3   echo "RESULT: ${currentBuild.result}"

    }
    
    
}