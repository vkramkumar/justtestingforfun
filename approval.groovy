node {
    stage ('Sample1') { 
        echo "Sample 1"
    }
    
    stage ('Sample2') {
        input message: 'Gavin, Kindly approve the build to proceed for stage3', ok: 'Head_to_PROD', submitter: 'g'
        input message: 'Yoliswa, Kindly approve the build to proceed for stage3', ok: 'Head_to_PROD', submitter: 'y'
    }

    stage ('Sample3') { 
        echo "Sample 3"
    }
    
}
