node {
 
    stage ('clean') {
        sh "rm -rf ${WORKSPACE}/* "
    }
    
    stage ('Folder') {
        sh "touch ${WORKSPACE}/${IAM_TEST}"
    }
    
    stage ('Bash') {
        git "https://vkramkumar@bitbucket.org/vkramkumar/justtestingforfun.git"
        sh "chmod 755 *.sh"
        sh "${WORKSPACE}/parameter_pass.sh ${folders}"
    }
}