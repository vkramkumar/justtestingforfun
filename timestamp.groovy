def notifySlack(String buildStatus = 'STARTED') {
    // Build status of null means success.
    buildStatus = buildStatus ?: 'SUCCESS'

    def color

    if (buildStatus == 'STARTED') {
        color = '#D4DADF'
    } else if (buildStatus == 'SUCCESS') {
        color = '#BDFFC3'
    } else if (buildStatus == 'UNSTABLE') {
        color = '#FFFE89'
    } else {
        color = '#FF9FA1'
    }

    def msg = "${buildStatus}: `${env.JOB_NAME}` #${env.BUILD_NUMBER}:\n${env.BUILD_URL}"


}

node {
    try {
        
        timestamps {

//            This is a Example One            
            ansiColor ('Example') {
                    try {
                    sh 'exit 0'
                    }
                    catch(ex) {
                    echo "'\033[1;32m'  This is GREEN printed error."
                    throw (ex)
                    }
                }



//
//          This piece iof code don't throw the shitty error  
            // stage('Some Stage') {
            //     try {
            //         sh make error
            //         //throw new Exception ("the build has failed")
            //     }
            //     catch(Exception e) {
            //         // squelch the exception, the pipeline will not fail
            //         // Above block not gonna run, it will throw error
            //         // but below catrch will except that nad make the build successfull
            //     }
            //     }
        }

        // Existing build steps.
    } catch (e) {
        currentBuild.result = 'FAILURE'
        throw e
    } finally {
        notifySlack(currentBuild.result)
    }
}