node {

BRANCH_NAME=DEV
stage('My Conditional Stage') {
    when (BRANCH_NAME != 'master') {
        echo 'Only on master branch.'
    }
}
}